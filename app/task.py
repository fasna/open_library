from celery import Celery
from app import celery
from app.models import Book
from flask import json
import time
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy




@celery.task()
def create_file_in_background():
    app = Flask(__name__)
    db = SQLAlchemy(app)
    app.config.from_object(Config)
    
    with app.app_context():
        d={}
        book_list = Book.query.all()
        for book in book_list:
            d[book.id]={}
            d[book.id]['title']=book.title
            d[book.id]['summary'] = book.summary
        file = open('a.json','w')
        file.write(json.dumps(d))
        file.close()

    return 'success'